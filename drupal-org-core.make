api = 2
core = 7.x

; MAKE file for Drupal core. 

; Drupal Core
projects[drupal][type] = core
projects[drupal][version] = 7.39

; *********** PATCHES ************

; Patch to allow install profile enabling to enable dependencies correctly.
projects[drupal][patch][1093420] = http://drupal.org/files/1093420-22.patch