# Drupal Camp Cape Town 2015

**This repository is the home of supporting materials for the Drupal Camp talk by Jason Legodi & Ryan Purchase.** 

The repo at https://bitbucket.org/ryanp101/alpha_devel/overview is also part of this demo.

### TALK TOPIC
Building Drupal with Drush make, git and “a little magic”
 
### SUMMARY
Do you find that every time you start a new Drupal project you end up performing the same boring and time consuming steps just to get started? Do you find that your sites use many of the same modules and libraries and it’s a real chore to keep them updated?

Our talk provides an overview of some principals and best practices for building and deploying Drupal projects we’ve picked up over the years. We’ll demonstrate how using Drush makefiles to manage dependencies, Git for version control, and Features for configuration management can remove boredom and accelerate site building. We’ll also show how these basic tools provide a foundation for streamlining deployment to multiple development, staging and production environments, also known as “Enterprise Drupal.”

This talk will benefit anyone looking to streamline their operations and improve the maintainability of their projects.