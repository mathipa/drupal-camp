// Add bootstrap icon after external links
(function ($) {
  $(function () { // document.reday
    $('.region-content a[href^="http"], .region-content a[href^="//"]').each(function(){
      if ($(this).attr('href').indexOf(window.location.hostname) == -1) {
        $(this).addClass('external');
        $(this).not(':has("img")').append('<span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>');
      }
    });
  });
})(jQuery);