<?php

/**
 * @file
 * template.php
 */
/**
 * Change toolbar menu to be a button group
 */
function wcg_bootstrap_menu_tree__menu_toolbar($variables) {
  return '<ul class="btn-group">' . $variables['tree'] . '</ul>';
}

/**
 * Change each toolbar menu item to be a button
 */
function wcg_bootstrap_menu_link__menu_toolbar($variables) {
  $element = $variables['element'];
  $element['#localized_options']['attributes']['class'][] = 'btn';
  $element['#localized_options']['attributes']['class'][] = 'btn-xs';
  
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Change footer menu to be an inline list
 */
function wcg_bootstrap_menu_tree__menu_footer($variables) {
  return '<div class="text-center"><ul class="menu list-inline">' . $variables['tree'] . '</ul></div>';
}


/**
 * Overrides theme_menu_link().
 *
 * This is taken from bootstrap_menu_link() in bootstrap theme bootstrap/templates/menu/menu-link.func.php
 * Purpose is to add in disabled parent menu item into sub-menu as firs item
 */
function wcg_bootstrap_menu_link__main_menu(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
  if ($element['#below']) {
    // Prevent dropdown functions from being added to management menu so it
    // does not affect the navbar module.
    if (($element['#original_link']['menu_name'] == 'management') && (module_exists('navbar'))) {
      $sub_menu = drupal_render($element['#below']);
    }
    elseif ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] == 1)) {
      // Add our own wrapper.
      unset($element['#below']['#theme_wrappers']);
      // Add parent link as sub-menu first item
      $add_below = array(
        '#theme' => 'menu_link__main_menu',
        '#attributes' => array('class' => array('first', 'leaf')),
        '#title' => $element['#title'],
        '#href' => $element['#href'],
        '#below' => '',
        '#original_link' => $element['#original_link'],
        '#localized_options' => array(),
      );
      array_unshift($element['#below'], $add_below);
      // Remove the class 'first' from the previous first item
      unset($element['#below'][1]['#attributes']['class'][0]);

      $sub_menu = '<ul class="dropdown-menu">' . drupal_render($element['#below']) . '</ul>';
      // Generate as standard dropdown.
      $element['#title'] .= ' <span class="caret"></span>';
      $element['#attributes']['class'][] = 'dropdown';
      $element['#localized_options']['html'] = TRUE;

      // Set dropdown trigger element to # to prevent inadvertant page loading
      // when a submenu link is clicked.
      $element['#localized_options']['attributes']['data-target'] = '#';
      $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
      $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
    }
  }
  // On primary navigation menu, class 'active' is not set on active menu item.
  // @see https://drupal.org/node/1896674
  if (($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#localized_options']['language']))) {
    $element['#attributes']['class'][] = 'active';
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Overrides theme_menu_link().
 *
 * Override the dropdowns created by wcg_bootstrap_menu_link__main_menu()
 */
function wcg_bootstrap_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  // On primary navigation menu, class 'active' is not set on active menu item.
  // @see https://drupal.org/node/1896674
  if (($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#localized_options']['language']))) {
    $element['#attributes']['class'][] = 'active';
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}