<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<?php if (!empty($page['above_header'])): ?>
<header class="hidden-xs hidden-sm" id="toolbar">
  <div class="container">
    <div class="col-sm-12 text-right">
      <?php print render($page['above_header']); ?>
    </div>
  </div>
</header>
<?php endif; ?>

<header class="hidden-xs hidden-sm" id="branding">
  <div class="container">
    <div class="col-sm-4">
      <?php if ($logo): ?>
      <a class="logo" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
        <img class="image-responsive" src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" >
      </a>
      <?php endif; ?>
    </div>
    <div class="col-sm-4 text-center">
      <?php if (!empty($site_name)): ?>
      <h3><a class="sitename" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a></h3>
      <?php endif; ?>
    </div>
    <div class="col-sm-4">
      <?php if (!empty($page['header'])): ?>
        <?php print render($page['header']); ?>
      <?php else: ?>
        <form>
          <div class="input-group">
            <input type="text" class="form-control">
            <span class="input-group-btn">
              <button class="btn btn-primary">Search</button>
            </span>
          </div>
        </form>
      <?php endif; ?>
    </div>
  </div>
</header>

<header id="navbar" role="banner" class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      
      <div class="visible-xs visible-sm">
        <a class="name navbar-brand logo pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><img src="<?php print $base_path;?><?php print drupal_get_path('theme', 'wcg_bootstrap'); ?>/images/logo-45x45.png" alt="<?php print t('Home'); ?>" > <?php print $site_name; ?></a>
      </div>
      
      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
      <button type="button" class="navbar-toggle text-center" data-toggle="collapse" data-target=".navbar-collapse" data-parent="#navbar-collapse-parent">
        <small>menu</small>
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
      <button data-target=".searchbar-collapse" data-parent="#navbar-collapse-parent" data-toggle="collapse" class="navbar-toggle searchbar-toggle" type="button">
          <span class="glyphicon glyphicon-search"></span>
        </button>
      
    </div>
    
    <div id="navbar-collapse-parent">
      <div class="panel">
        <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
          <div class="navbar-collapse collapse">
            <nav role="navigation">
              <?php if (!empty($primary_nav)): ?>
                <?php print render($primary_nav); ?>
              <?php endif; ?>
            </nav>
          </div>
        <?php endif; ?>
        
        <div class="searchbar-collapse collapse hidden-sm hidden-md hidden-lg">
          <form>
            <div class="input-group">
              <input type="text" class="form-control">
              <span class="input-group-btn">
                <button class="btn btn-primary">Search</button>
              </span>
            </div>
          </form>
        </div>
      </div>
    </div>
    
  </div>
</header>

<div class="main-container">
  <div class="container">
    <div class="row">

    <section class="col-sm-12">
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      
      <?php if (!empty($tabs)) print render($tabs); ?>
      <a id="main-content"></a>
      
      <?php print render($title_prefix); ?>
      <?php if (!empty($title)): ?>
        <h1 class="page-header"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      
      <?php print $messages; ?>
      
      <?php if (!empty($page['help'])) print render($page['help']); ?>
      
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      
      <?php print render($page['content']); ?>
      
    </section>
  </div>
  </div>
</div>
<footer id="footer">
  <?php if (!empty($page['footer_top'])): ?>
  <div class="container hidden-xs hidden-sm">
    <?php print render($page['footer_top']); ?>
  </div>
  <?php endif; ?>
  <div class="container">
    <?php print render($page['footer']); ?>
  </div>
  <div class="container">
    <section class="row">
      <div class="col-xs-12 col-sm-6 col-sm-push-3 text-center">
        <p>Western Cape Government &copy; <?php print date('Y')?>. All rights reserved.</p>
      </div>
      <div class="col-xs-6 col-sm-3 col-sm-pull-6 text-center">
        <a href="https://westerncape.gov.za/"><img class="image-responsive" src="<?php print $base_path;?><?php print drupal_get_path('theme', 'wcg_bootstrap');?>/images/logo-white.png" alt="Western Cape Government"></a>
      </div>
      <div class="col-xs-6 col-sm-3 text-center">
        <a href="http://www.gov.za/" target="_blank"><img class="image-responsive" src="<?php print $base_path;?><?php print drupal_get_path('theme', 'wcg_bootstrap');?>/images/coat-of-arms-flag.png" alt="South African Government"></a>
      </div>
    </section>
  </div>
</footer>
