<?php

/**
 * @file
 * template.php
 */
function wcg_bootstrap_base_preprocess_html($variables) {
  //only include if user is logged in and may see the navbar
  if(user_is_logged_in()){
    drupal_add_css(drupal_get_path('theme', 'wcg_bootstrap_base') . '/less/admin.less');
  }
}