/*
 * This file is used/requested by the 'Styles' button.
 * The 'Styles' button is not enabled by default in DrupalFull and DrupalFiltered toolbars.
 */
if(typeof(CKEDITOR) !== 'undefined') {
  CKEDITOR.addStylesSet( 'drupal',
  [
  /* Bootstrap Styles */

  /* Typography */
    { name : 'Text like H1'        , element : 'span', attributes: { 'class': 'h1' } },
    { name : 'Text like H2'        , element : 'span', attributes: { 'class': 'h2' } },
    { name : 'Text like H3'        , element : 'span', attributes: { 'class': 'h3' } },
    { name : 'Text like H4'        , element : 'span', attributes: { 'class': 'h4' } },
    { name : 'Text like H5'        , element : 'span', attributes: { 'class': 'h5' } },
    { name : 'Text like H6'        , element : 'span', attributes: { 'class': 'h6' } }, 

    { name : 'Lead Paragraph'     , element : 'p', attributes: { 'class': 'lead' } },

    {
      name : 'Well',
      element : 'div',
      attributes :
      {
        'class' : 'well'
      }
    },
    {
      name : 'Unstyled List',
      element : 'ul',
      attributes :
      {
        'class' : 'list-unstyled'
      }
    },
    {
      name : 'List inline',
      element : 'ul',
      attributes :
      {
        'class' : 'list-inline'
      }
    },
    {
      name : 'Table Striped rows',
      element : 'table',
      attributes :
      {
        'class' : 'table table-striped'
      }
    },
    {
      name : 'Table Bordered',
      element : 'table',
      attributes :
      {
        'class' : 'table table-bordered'
      }
    },
    {
      name : 'Table Condensed',
      element : 'table',
      attributes :
      {
        'class' : 'table table-condensed'
      }
    },                
    {
      name : 'Image shape rounded',
      element : 'img',
      attributes :
      {
        'class' : 'img-rounded'
      }
    },
    {
      name : 'Image shape circle',
      element : 'img',
      attributes :
      {
       'class' : 'img-circle'
      }
    },
  ]);
}