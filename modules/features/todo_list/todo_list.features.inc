<?php
/**
 * @file
 * todo_list.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function todo_list_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function todo_list_node_info() {
  $items = array(
    'todos' => array(
      'name' => t('ToDos'),
      'base' => 'node_content',
      'description' => t('A ToDo List content type provided by a Feature module.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
