<?php
/**
 * @file
 * todo_list.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function todo_list_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'body'
  $field_bases['body'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'node',
    ),
    'field_name' => 'body',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_with_summary',
  );

  // Exported field_base: 'field_todo_s'
  $field_bases['field_todo_s'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_todo_s',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'mytinytodo',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'mytinytodo_list',
  );

  return $field_bases;
}
