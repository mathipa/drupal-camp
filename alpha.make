api = 2
core = 7.x

; MODULES

projects[ctools][version] = 1.9
projects[ctools][subdir] = contrib

projects[diff][version] = 3.2
projects[diff][subdir] = contrib

projects[features][version] = 2.6
projects[features][subdir] = contrib

projects[strongarm][version] = 2.0
projects[strongarm][subdir] = contrib

projects[jquery_update][version] = 3.0-alpha2
projects[jquery_update][subdir] = contrib

projects[less][version] = 4.0
projects[less][type] = module
projects[less][subdir] = contrib

projects[libraries][version] = 2.2
projects[libraries][subdir] = contrib
; Let modules add library paths
projects[libraries][patch][] = https://www.drupal.org/files/issues/add_libraries_directories_alter-2334991-2.patch

projects[module_filter][version] = 2.0
projects[module_filter][subdir] = contrib

projects[mytinytodo][version] = 1.1
projects[mytinytodo][subdir] = contrib

projects[navbar][version] = 1.6
projects[navbar][subdir] = contrib

projects[pathauto][version] = 1.2
projects[pathauto][subdir] = contrib

projects[token][version] = 1.6
projects[token][subdir] = contrib

projects[variable][version] = 2.5
projects[variable][subdir] = contrib

projects[views][version] = 3.11
projects[views][subdir] = contrib

; THEMES

projects[bootstrap][version] = 3.1-beta3
projects[bootstrap][type] = theme

projects[adminimal_theme][version] = 1.22
projects[adminimal_theme][type] = theme

; LIBRARIES

libraries[backbone][download][type] = git
libraries[backbone][download][url] = https://github.com/jashkenas/backbone.git
libraries[backbone][download][tag] = 1.0.0

libraries[bootstrap][download][type] = "get"
libraries[bootstrap][download][url] = https://github.com/twbs/bootstrap/archive/v3.3.2.zip
libraries[bootstrap][directory_name] = bootstrap
libraries[bootstrap][type] = library

libraries[less.php][download][type] = get
libraries[less.php][download][url] = https://github.com/oyejorge/less.php/releases/download/v1.7.0.2/less.php_1.7.0.2.zip
libraries[less.php][directory_name] = less.php
libraries[less.php][type] = library

libraries[modernizr][download][type] = git
libraries[modernizr][download][url] = https://github.com/BrianGilbert/modernizer-navbar.git
libraries[modernizr][download][revision] = 5b89d9225320e88588f1cdc43b8b1e373fa4c60f

libraries[underscore][download][type] = git
libraries[underscore][download][url] = https://github.com/jashkenas/underscore.git
libraries[underscore][download][tag] = 1.5.0
