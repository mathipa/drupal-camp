#!/bin/bash

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  Demonstration build script for Drupal Camp Cape Town - 2015
#
#  This program is free software. You can redistribute it and/or modify it under
#  the terms of the GNU GPL as published by the Free Software Foundation, version 2
#  or later.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU GPL for more details.
#
#  Code: https://bitbucket.org/mathipa/drupal-camp
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CONFIG DEFAULTS - Modify the configuration in this section
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Default environment if -e option is not passed to script. 
# Set to "prod" or "dev"
env=prod
# Defatul site lable if argument is not passed to script. 
# Used for folder and database name.
site="alpha" 
# Root directory for local web server
web_root="/Users/ryan/Sites"
# Set to "0" if your local server does not handle vhosts automatically.
# If set to 1, this script assumes vhost will be http://$site.dev
# If set to 0, this script assumes the site will be served from http://localhost/$site
vhost=1
# Database settings
# NOTE: Generally these would be set by environment variables and not hard-coded
# into this script which is hosted in a public repository!
db_host="localhost"
db_name="alpha"
db_un="root"
db_pw="root"
# Email address for Drupal site admin. All system generated emails will be rerouted
# to this email when building a development instance.
admin_email="ryan.purchase@westerncape.gov.za"
repo="drupal-camp"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# DON'T EDIT ANYTHING BELOW THIS LINE    
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
migrate=0
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# HELP OUTPUT WHEN -h OPTION IS PASSED IN
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function show_usage () {
  cat << EOF

usage: build [-e|--environment <dev|prod>] <site-lable> <repo-lable>

Build and install a site for a specific environment into a folder called "lable".

example: 
build -e dev test
will build and install a development instance of the site into <webroot>/test

EOF
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# PARSE SCRIPT OPTIONS & ARGUMENTS
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

while [[ $# > 2 ]]
do
key="$1"

shift
case $key in
  -e|--environment)
    if [[ "$1" = "dev" ]] || [[ "$1" = "prod" ]]; then
      env="$1"
    else
      echo "[error] Unrecognised environment - $1"
      show_usage
      exit 1
    fi
    shift
  ;;
  -h|--help)
    show_usage
    exit 1
    shift
  ;;
  *)
    # unknown option
  ;;
esac
done

# override site lable/folder if argument provided
if [ $1 ]; then
  site="$1"
fi 

# override site lable/folder if argument provided
if [ $2 ]; then
  repo="$2"
fi 

# other script variables

# TODO: implement parsing of other popular git host uri's
repo="https://bitbucket.org/mathipa/$repo"
install_dir="$web_root/$site"
db_name="$site"
# Remove problematic characters from db name: .- 
db_name="${db_name//-/_}"
db_name="${db_name//./_}"

if [ $vhost = 1 ]; then
  site_uri="http://$site.dev"
else
  site_uri="http://localhost/$site"
fi

echo "$(tput setaf 6)## Environment = $env$(tput sgr0)"
echo "$(tput setaf 6)## DB Name = $db_name$(tput sgr0)"
echo "$(tput setaf 6)## Site lable = $site$(tput sgr0)"
echo "$(tput setaf 6)## Site URI = $site_uri$(tput sgr0)"
echo "$(tput setaf 6)## Repo URI = $repo$(tput sgr0)"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# PROMPT TO REMOVE EXISTING SITE FOLDER
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if [ -d "$install_dir" ]; then

  if [ "$env" = "prod" ]; then
    # tear down prod build
    # tear down dev site
    echo "$(tput setaf 1)## About to tear down existing production site. This will also trash the existing database for this site.$(tput sgr0)"
    select opt in "Migrate this site" "Fresh build" "Cancel"; do
        case $opt in
            "Migrate this site" ) 
              # create site archive
              cwd="$(pwd)"
              cd $install_dir
              echo "$(tput setaf 6)## Backing up the site so it can be restored later.$(tput sgr0)"
              archive="$(drush archive-dump --pipe)"
              echo "$(tput setaf 6)## Backed up to $archive$(tput sgr0)"
              migrate=1
              migrate_site=$site
              cd "$cwd"
              break;;
            "Fresh build" ) 
              break;;
            "Cancel" )
              exit;;
        esac
    done
  else
    # tear down dev site
    echo "$(tput setaf 1)## About to tear down existing developnent site. This will also trash the existing database for this site.$(tput sgr0)"
    select opt in "Migrate this site" "Migrate from another site" "Fresh build" "Cancel"; do
        case $opt in
            "Migrate this site" ) 
              # create site archive
              cwd="$(pwd)"
              cd $install_dir
              echo "$(tput setaf 6)## Backing up the site so it can be restored later.$(tput sgr0)"
              archive="$(drush archive-dump --pipe)"
              echo "$(tput setaf 6)## Backed up to $archive$(tput sgr0)"
              cd "$cwd"
              migrate=1
              migrate_site=$site
              break;;
            "Migrate from another site" )
              read -r -p "Which site? Enter an existing site lable > " response
              if [ -d "$web_root/$response" ]; then
                cwd="$(pwd)"
                cd "$web_root/$response"
                echo "$(tput setaf 6)## Preparing $response archive so it can be restored later.$(tput sgr0)"
                archive="$(drush archive-dump --pipe)"
                echo "$(tput setaf 6)## Backed up to $archive$(tput sgr0)"
                cd "$cwd"
                migrate=1
                migrate_site=$response
              else
                echo "$(tput setaf 1)[error] Site $response not found!$(tput sgr0)"
                exit
              fi
              break;;
            "Fresh build" ) 
              break;;
            "Cancel" ) 
              exit;;
        esac
    done
  fi
  # delete site folder
  chmod -R 755 $install_dir
  rm -rf $install_dir
  echo "$(tput setaf 1)## Deleted existing site$(tput sgr0)"
else
  # prompt to migrate for new dev builds
  if [ "$env" = "dev" ]; then
    # tear down prod build
    # tear down dev site
    echo "$(tput setaf 1)## About to build a new site.$(tput sgr0)"
    select opt in "Fresh build" "Migrate from another site" "Cancel"; do
        case $opt in
            "Migrate from another site" ) 
              read -r -p "Which site? Enter an existing site lable > " response
              if [ -d "$web_root/$response" ]; then
                cwd="$(pwd)"
                cd "$web_root/$response"
                echo "$(tput setaf 6)## Preparing $response archive so it can be restored later.$(tput sgr0)"
                archive="$(drush archive-dump --pipe)"
                echo "$(tput setaf 6)## Backed up to $archive$(tput sgr0)"
                cd "$cwd"
                migrate=1
                migrate_site=$response
              else
                echo "$(tput setaf 1)[error] Site $response not found!$(tput sgr0)"
                exit
              fi
              break;;
            "Fresh build" ) 
              exit;;
            "Cancel" )
              exit;;
        esac
    done
  fi
fi

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# BUILD THE SITE USING DRUSH MAKE
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# choose remote makefile based on $env variable
makefile="$repo/raw/master/build.$env.make"
echo "$(tput setaf 6)## Building site into $install_dir$(tput sgr0)"
# execute drush make to build the site into the install directory using the environment makefile
# NOTE: --only-once option is to bump up caching for presentation purposes. It also requires 
# a custom drush hook - see http://linuxdev.dk/blog/speed-drush-make
drush make --concurrency=8 --only-once --strict=0 $makefile $install_dir --working-copy

if [ $? -ne 0 ]; then
  echo "$(tput setaf 1)[error] Failed to build site from makefile$(tput sgr0)"
  exit 1
fi
# change working directory
cd $install_dir

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# MAKE EDITS to .htaccess
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if [ $vhost = 1 ]; then
  # uncomment RewriteBase / if using a vhost
  echo "$(tput setaf 6)## Setting RewriteBase in .htaccess$(tput sgr0)"
  sed 's/# RewriteBase \/$/RewriteBase \//g' <.htaccess >tmp.htaccess && mv tmp.htaccess .htaccess
fi

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# INSTALL THE SITE WITH DRUSH SITE INSTALL
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo "$(tput setaf 6)## Installing $site_uri using the alpha profile$(tput sgr0)"
drush --uri="$site_uri" si -y alpha --account-mail="$admin_email" --site-mail="$admin_email" --db-url=mysql://$db_un:$db_pw@$db_host/$db_name;

if [ $? -ne 0 ]; then
  echo "$(tput setaf 1)[error] Failed to install $site_uri$(tput sgr0)"
  exit 1
fi

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# RESTORE DB BACKUP IF MIGRATE FLAG IS SET
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# migrate site databse and content
if [ "$migrate" = 1 ]; then
  echo "$(tput setaf 6)## Restoring backup from $archive$(tput sgr0)"

  # migrate assets - assumes they are in sites/default/files
  echo "$(tput setaf 6)## Restoring assets from $archive backup$(tput sgr0)"
  tar -zxvf "$archive" -C $install_dir "$migrate_site/sites/default/files"

  # Remove problematic characters from db name: .- 
  migrate_site="${migrate_site//-/_}"
  migrate_site="${migrate_site//./_}"
  cwd="$(pwd)"
  tmp_dir="/tmp/build_alpha_tmp_""$migrate_site""_"`date +"%Y%m%d%H%M""-$RANDOM"`
  mkdir "$tmp_dir"
  tar -zxf "$archive" -C "$tmp_dir" "$migrate_site.sql" 
  cd $install_dir

  echo "$(tput setaf 6)## Restoring database from $archive backup$(tput sgr0)"
  drush sql-drop -y
  drush sql-cli < "$tmp_dir/$migrate_site.sql"

  # revert all features and run database updates
  drush --uri="$site_uri" fra -y
  drush updb

  cd "$cwd"
else
  cwd="$(pwd)"
  cd $install_dir
  drush cc all
  drush --uri="$site_uri" fra -y
  drush updb
  cd "$cwd"
fi

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ENABLE THE CUSTOM DEV MODULE IF DEV BUILD
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if [ "$env" = "dev" ]; then
  drush en alpha_devel -y
fi

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# LOG INTO SITE AND LAUNCH BROWSER WITH DRUSH ULI
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

tput setaf 6
# Log into site and launch in browser 
drush --uri="$site_uri" uli
tput sgr0
