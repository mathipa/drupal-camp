; dev build

api = 2
core = 7.x

; Include the definition for how to build Drupal core directly, including patches:
includes[core] = https://bitbucket.org/mathipa/drupal-camp/raw/master/drupal-org-core.make

; Download the alpha install profile and recursively build all its dependencies:
projects[alpha][type] = profile
projects[alpha][download][type] = git
projects[alpha][download][branch] = master
projects[alpha][download][url] = git@bitbucket.org:mathipa/drupal-camp.git