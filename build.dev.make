; dev build

core = 7.x
api = 2

; Include the definition for how to build Drupal core directly, including patches:
includes[core] = https://bitbucket.org/mathipa/drupal-camp/raw/master/drupal-org-core.make

; Include custom development module
projects[alpha_devel][download][type] = git
projects[alpha_devel][download][branch] = master
projects[alpha_devel][download][url] = https://ryanp101@bitbucket.org/ryanp101/alpha_devel.git
projects[alpha_devel][type] = module
projects[alpha_devel][subdir] = custom


; Download the alpha install profile and recursively build all its dependencies:
projects[alpha][type] = profile
projects[alpha][download][type] = git
projects[alpha][download][branch] = master
projects[alpha][download][url] = https://ryanp101@bitbucket.org/mathipa/drupal-camp.git